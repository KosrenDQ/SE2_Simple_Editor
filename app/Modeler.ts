//---------------------IMPORTS---------------------
import '../assets/scss/main.scss';
import { Viewer } from './Viewer';
import { Options } from 'diagram-js';

//---------------------CONSTANTS---------------------
const DEFAULT_PRIORITY = 1000;
const DEFAULT_OPTIONS: Options = {
  width: '100%',
  height: '100%',
  position: 'relative',
  container: 'body'
};
const DEFAULT_MODULES = [
  // modeling components
  require('diagram-js/lib/features/auto-scroll'),
  require('diagram-js/lib/features/hand-tool'),
  require('diagram-js/lib/features/lasso-tool'),
  require('diagram-js/lib/features/move'),
  require('diagram-js/lib/features/resize'),
  require('diagram-js/lib/features/space-tool'),

  require('./features/context-pad'),
  require('./features/label-editing'),
  require('./features/modeling'),
  require('./features/palette'),
  require('./features/properties-panel'),
  require('./features/rules'),
  require('./features/snapping')
];

//---------------------CLASS--------------------
export class Modeler extends Viewer {

  //---------------------CONSTRUCTOR---------------------
  constructor(options?: Options) {
    options = {...DEFAULT_OPTIONS, ...options};
    super(options, DEFAULT_MODULES);
    this.on('import.parse.complete', DEFAULT_PRIORITY, (event) => {
      if (!event.error) {
        this.collectIds(event.definitions, event.context);
      }
    }, this);
    this.on('diagram.destroy', DEFAULT_PRIORITY, () => {
      this.moddle.ids.clear();
    }, this);
  }
}
