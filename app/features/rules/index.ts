//---------------------IMPORTS---------------------
import EasyRules from './EasyRules';

//---------------------EXPORTS---------------------
module.exports = {
  __depends__: [ require('diagram-js/lib/features/rules') ],
  __init__: [ 'easyRules' ],
  easyRules: [ 'type', EasyRules ]
};
