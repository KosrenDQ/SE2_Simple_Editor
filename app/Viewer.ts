//---------------------IMPORTS---------------------
import * as DiagramJS from 'diagram-js';
import { assign, omit } from 'min-dash';
import { domify, query } from 'min-dom';
import Ids = require('ids');

import EasyModdle from './EasyModdle';
import { importEasyDiagram } from './import/Importer';

//---------------------CONSTANTS---------------------
const DEFAULT_PRIORITY = 1000;
const DEFAULT_OPTIONS = {
  width: '100%',
  height: '100%',
  position: 'relative',
};
const DEFAULT_MODULES = [
  // modeling components
  require('./draw'),
  require('./import'),
  // modules the viewer is composed of
  require('diagram-js/lib/features/overlays'),
  require('diagram-js/lib/features/selection'),
  // non-modeling components
  require('diagram-js/lib/navigation/movecanvas'),
  require('diagram-js/lib/navigation/touch'),
  require('diagram-js/lib/navigation/zoomscroll'),
];

//---------------------CLASS--------------------
export class Viewer extends DiagramJS {
  protected moddle: any;
  protected container: any;
  protected definitions: any;
  protected modules;

  //---------------------CONSTRUCTOR---------------------
  constructor(options?: DiagramJS.Options, modules?: any[]) {
    options = assign({}, DEFAULT_OPTIONS, options);
    super(options);

    this.modules = DEFAULT_MODULES.concat(modules);
    this.moddle = this.createModdle(options);
    this.container = this.createContainer(options);
    this.init(this.container, this.moddle, options);
  }

  //---------------------METHODS---------------------
  getModules() {
    return this.modules;
  }

  on(event, priority, callback, target) {
    return this.get('eventBus').on(event, priority, callback, target);
  }

  importXML(xml, done: (err, warnings?) => void = function() {}) {
    const self = this;
    xml = this.emit('import.parse.start', { xml: xml }) || xml;
    console.log('Start Parsing!');
    this.moddle.fromXML(xml, 'ea:EasyGraph', {}, function(err, definitions, context) {
      console.log('Successfully parsed!');
      definitions = self.emit('import.parse.complete', {
        error: err,
        definitions: definitions,
        context: context
      }) || definitions;
      if(err) {
        err = this.checkValidationError(err);
        self.emit('import.done', { error: err });
        return done(err);
      }
      const parseWarnings = context.warnings;
      self.importDefinitions(definitions, function(err, importWarnings) {
        const allWarnings = [].concat(parseWarnings, importWarnings || []);
        self.emit('import.done', { error: err, warnings: allWarnings });
        done(err, allWarnings);
      });
    });
  }

  importDefinitions(definitions, done) {
    try {
      if(this.definitions) {
        this.clear();
      }
      this.definitions = definitions;
      importEasyDiagram(this, definitions, done);
    } catch(e) {
      done(e);
    }
  }

  protected init(container: any, moddle: any, options: DiagramJS.Options) {
    const baseModules = options.modules || this.getModules();
    const additionalModules = options.additionalModules || [];
    const staticModules: any[] = [
      {
        moddle: ['value', moddle]
      }
    ];
    const diagramModules = staticModules.concat(baseModules, additionalModules);
    const diagramOptions = assign(omit(options, ['additionalModules']), {
      canvas: assign({}, options.canvas, {container: container}),
      modules: diagramModules
    });
    // invoke diagram constructor
    DiagramJS.call(this, diagramOptions);
    if (options && options.container) {
      this.attachTo(options.container);
    }
  }

  protected emit(type: string, event: any) {
    return this.get('eventBus').fire(type, event);
  }

  protected createModdle(options) {
    const moddle = new EasyModdle(options.moddleExtensions);
    moddle.ids = new Ids([ 32, 36, 1 ]);
    return moddle;
  }

  protected createContainer(options: DiagramJS.Options) {
    const container = domify('<div class="diagram-container"></div>');
    assign(container.style, {
      width: options.width,
      height: options.height,
      position: options.position
    });
    return container;
  }

  protected attachTo(parentNode) {
    if (!parentNode) {
      throw new Error('parentNode required');
    }
    // ensure we detach from the
    // previous, old parent
    this.detach();
    // unwrap jQuery if provided
    if (parentNode.get && parentNode.constructor.prototype.jquery) {
      parentNode = parentNode.get(0);
    }
    if (typeof parentNode === 'string') {
      parentNode = query(parentNode);
    }
    parentNode.appendChild(this.container);
    this.emit('attach', {});
    this.get('canvas').resized();
  }

  protected detach() {
    const parentNode = this.container.parentNode;
    if (!parentNode) {
      return;
    }
    this.emit('detach', {});
    parentNode.removeChild(this.container);
  }

  protected collectIds(definitions, context) {
    const moddle = definitions.$model;
    const ids: Ids = moddle.ids;
    ids.clear();
    for(let id of context.elementsById) {
      ids.claim(id, context.elementsById[id]);
    }
  }

  protected checkValidationError(err) {
    const pattern = /unparsable content <([^>]+)> detected([\s\S]*)$/;
    const match = pattern.exec(err.message);
    if (match) {
      err.message = 'Unparsable content <' + match[1] + '> detected;' +
                    'check your definition file' + match[2];
    }
    return err;
  }
}
