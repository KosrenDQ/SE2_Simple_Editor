//---------------------IMPORTS---------------------
import { assign, isObject } from 'lodash';
import { is } from '@utils/ModelUtil';
import { createLine } from 'diagram-js/lib/util/RenderUtil';
import BaseRenderer = require('diagram-js/lib/draw/BaseRenderer');
import TextUtil = require('diagram-js/lib/util/Text');
import { query as domQuery } from 'min-dom';
import { append as svgAppend } from 'tiny-svg';
import { attr as svgAttr } from 'tiny-svg';
import { create as svgCreate } from 'tiny-svg';
import { clear as svgClear } from 'tiny-svg';
import { classes as svgClasses } from 'tiny-svg';

//---------------------CONSTANTS---------------------
const LABEL_STYLE = {
  fontFamily: 'Arial, sans-serif',
  fontSize: '12px'
};

//---------------------CLASS--------------------
export default class EasyRenderer extends BaseRenderer {
  //---------------------ATTRIBUTES---------------------
  markers = {};
  computeStyle: any;
  textUtil: any;
  handlers: any;
  canvas: any;

  //---------------------CONSTRUCTOR---------------------
  constructor(eventBus, styles, canvas, priority) {
    super(eventBus, priority);
    this.textUtil = new TextUtil({
      style: LABEL_STYLE,
      size: {width: 100}
    });
    this.canvas = canvas;
    this.computeStyle = styles.computeStyle;
    const CONNECTION_STYLE = styles.style(['no-fill'], {strokeWidth: 2, stroke: 'black'});
    this.initMarker();
    const STYLE = styles.style({
      fillOpacity: 1,
      stroke: 'black',
      strokeWidth: 2
    });
    this.handlers = {
      'ea:Node': (visuals, element) => {
        const rect = this.drawRect(visuals, element.width || 0, element.height || 0, STYLE);
        this.renderEmbeddedLabel(visuals, element, 'center-top');
        return rect;
      },
      'ea:Edge': (visuals, element) => {
        return this.drawLine(visuals, element.waypoints, CONNECTION_STYLE);
      }
    };
  }

  //---------------------METHODS---------------------
  addMarker(id, element) {
    this.markers[id] = element;
  }

  marker(id) {
    return this.markers[id];
  }

  getMarker(id) {
    return `url(#${id})`;
  }

  initMarker() {
    const self = this;
    function createMarker(id, options) {
      const attrs = assign({
        fill: 'black',
        strokeWidth: 1
      }, options.attrs);
      const ref = options.ref || { x: 0, y: 0 };
      const scale = options.scale || 1;
      if(attrs.strokeDasharray === 'none') {
        attrs.strokeDasharray = [10000, 1];
      }
      const marker = svgCreate('marker');
      svgAttr(options.element, attrs);
      svgAppend(marker, options.element);
      svgAttr(marker, {
        id: id,
        viewBox: '0 0 20 20',
        refX: ref.x,
        refY: ref.y,
        markerWidth: 20 * scale,
        markerHeight: 20 * scale,
        orient: 'auto'
      });
      let defs = domQuery('defs', self.canvas._svg);
      if(!defs) {
        defs = svgCreate('defs');
        svgAppend(self.canvas._svg, defs);
      }
      svgAppend(defs, marker);
      self.addMarker(id, marker);
    }

    const path1 = svgCreate('path');
    svgAttr(path1, { d: 'M 1 5 L 11 10 L 1 15 M 1 15 L 1 5' });
    createMarker('connection-end-none', {
      element: path1,
      attrs: {
        fill: 'white',
        stroke: 'slategray',
        strokeWidth: 1
      },
      ref: { x: 12, y: 10 },
      scale: 1
    });
  }

  drawRect(visuals, width, height, offset, attrs?) {
    if(isObject(offset)) {
      attrs = offset;
      offset = 0;
    }
    offset = offset || 0;
    attrs = this.computeStyle(attrs, {
      stroke: 'black',
      strokeWidth: 2,
      fill: 'white'
    });
    const rect = svgCreate('rect');
    svgAttr(rect, {
      x: offset,
      y: offset,
      width: width - offset * 2,
      height: height - offset * 2,
    });
    svgAttr(rect, attrs);
    svgAppend(visuals, rect);
    return rect;
  }

  drawLine(p, waypoints, attrs?) {
    attrs = this.computeStyle(attrs, [ 'no-fill' ], {
      stroke: 'slategray',
      strokeWidth: 2,
      fill: 'white'
    });
    const line = createLine(waypoints, attrs);
    svgAppend(p, line);
    return line;
  }

  renderEmbeddedLabel(p, element, align) {
    return this.renderLabel(p, element.businessObject.name, { box: element, align: align, padding: 5 });
  }

  renderLabel(p, label, options) {
    const text = this.textUtil.createText(label || '', options);
    svgClasses(text).add('djs-label');
    svgAppend(p, text);
    return text;
  }

  canRender(element): boolean {
    return is(element, 'ea:Element') || is(element, 'label');
  }

  drawShape(visuals, element) {
    const handler = this.handlers[element.type];
    return handler(visuals, element);
  }

  drawConnection(visuals, element) {
    const handler = this.handlers[element.type];
    return handler(visuals, element);
  }
}

(EasyRenderer as any).$inject = ['eventBus', 'styles', 'canvas'];
